import { Pipe, PipeTransform } from '@angular/core';
import *as moment from 'moment';

@Pipe({
    name: 'clock'
})

export class ClockPipe implements PipeTransform {
    transform(value: number) {
        return moment.unix(value).format('HH:mm');
    }
}