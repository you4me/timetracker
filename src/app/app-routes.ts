import { Route } from '@angular/router';
import { HomePageComponent} from './pages';


export const routes: Route[] = [
{
    path:'',
    redirectTo: 'home',
    pathMatch: 'full'
},
{
    path: 'home',
    component: HomePageComponent
}

];

