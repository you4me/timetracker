import { Component } from "@angular/core";
import { TimeService } from "../../services/time.service";

@Component({
    selector: 'home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss']
})

export class HomePageComponent {

    public currentTime: number;

    constructor(private ts: TimeService){}

    ngOnInit(): void {
        this.ts.timerEmitter.subscribe(currentTime => {
           this.currentTime =  currentTime
        });
    }
}