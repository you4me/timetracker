import { NgModule } from "@angular/core";
import { HomePageComponent} from './'
import { CommonModule } from '@angular/common'
import { ClockPipe } from "../pipes/clock.pipe";

@NgModule({
    declarations: [
        HomePageComponent,
        ClockPipe
    ],
    exports: [
        HomePageComponent
    ],
    imports: [
        CommonModule
    ]
})

export class PagesModule {}