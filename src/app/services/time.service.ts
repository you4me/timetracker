import { Injectable } from "@angular/core";
import * as moment from 'moment';
import { Subject } from "rxjs";

@Injectable()

export class TimeService{

   
    private readonly second: number = 1000;

    public timerEmitter: Subject<number> = new Subject();

    constructor(){
        this.startTimer();
    }

    private startTimer(): void {
        setInterval(() => {
            this.timerEmitter.next(moment().unix());
   
           }, this.second);
    }
}

